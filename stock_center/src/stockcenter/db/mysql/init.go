package mysql

import (
	"database/sql"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/smmit/smmbase/logger"
)

// var engine *xorm.Engine

// func Init() {
// 	var err error
// 	engine, err = xorm.NewEngine("mysql", "root:@/test?charset=utf8")
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	//连接测试
// 	if err := engine.Ping(); err != nil {
// 		log.Fatal(err)
// 		return
// 	}

// 	//日志打印SQL
// 	engine.ShowSQL(true)

// 	//设置连接池的空闲数大小
// 	engine.SetMaxIdleConns(10)
// 	//设置最大打开连接数
// 	engine.SetMaxOpenConns(50)

// }

var (
	dbi *sqlx.DB
)

//InitDB 初始化数据库
func InitDB() (err error) {

	connStr := fmt.Sprintf("root:root@tcp(127.0.0.1:3306)/test" + "?parseTime=true&loc=Local")
	dbi, err = sqlx.Open("mysql", connStr)
	if err != nil {
		logger.Error(err)
		return err
	}

	err = dbi.Ping()
	if err != nil {
		logger.Error(err)
		return err
	}

	dbi.SetMaxOpenConns(50)
	dbi.SetMaxIdleConns(10)
	dbi.SetConnMaxLifetime(time.Second * 300)

	return nil
}

//InitDB 初始化数据库
func GetDB() *sqlx.DB {
	return dbi
}

//getInsertID 获取插入的自增键值
func getInsertID(res sql.Result) int {
	id, err := res.LastInsertId()
	if err != nil {
		logger.Error(err)
	}

	return int(id)
}

//Transaction 封装的事务对象，有两种用法：
//	1、使用 defer tx.End() 方法自动操作，操作方式(提交或回滚)由 SetResult() 方法指定
//	2、直接使用 Commit() 或 RollBack() 操作，操作后 End() 方法将不会再次进行提交或回滚
//	建议使用方式：使用 defer tx.End() 以便随时使用return，事务将默认自动回滚。在函数正常结束时，使用 Commit() 获取提交结果
type Transaction struct {
	tx      *sqlx.Tx
	success bool
	valid   bool
}

//SetResult 设置结果
func (tx *Transaction) SetResult(result bool) {
	tx.success = result
}

//End 执行或回滚事务
func (tx *Transaction) End() (err error) {
	if !tx.valid { //如果事务已经被提交或回滚了，则无需再次操作
		return nil
	}
	if tx.success {
		err = tx.tx.Commit()
		if err != nil {
			logger.Error(err)
		}
	} else {
		err = tx.tx.Rollback()
		if err != nil {
			logger.Error(err)
		}
	}
	if err != nil {
		logger.Error(err)
	}
	return
}

//Commit 主动提交事务
func (tx *Transaction) Commit() error {
	tx.valid = false
	err := tx.tx.Commit()
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

//RollBack 主动回滚事务
func (tx *Transaction) RollBack() error {
	tx.valid = false
	err := tx.tx.Rollback()
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

//GetTx 获取数据库事务
func (tx *Transaction) GetTx() *sqlx.Tx {
	return tx.tx
}

//CreateTx 创建事务
func CreateTx() (tx *Transaction, err error) {
	newTx, err := dbi.Beginx()
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	tx = &Transaction{
		tx:      newTx,
		success: false,
		valid:   true,
	}
	return
}
