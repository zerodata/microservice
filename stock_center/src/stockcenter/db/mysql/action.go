package mysql

import (
	"bytes"
	"fmt"
	"log"
	"stockcenter/proto"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/smmit/smmbase/logger"
)

//库存
type Stock struct {
	ID          int    `db:"id"`
	StockName   string `db:"stock_name"`
	ParentID    int    `db:"parent_id"`
	ParentName  string `db:"parent_name"`
	StockAmount int    `db:"stock_amount"`
	Status      string `db:"status"`
	CreateTime  int    `db:"create_time"`
	UpdateTime  int    `db:"update_time"`
	Creator     string `db:"creator"`
}

func InsertStock(tx *Transaction, reqParam *Stock) (err error) {
	sqlStr := fmt.Sprintf(`
	insert into %s
	(
		stock_name,
		stock_level,
		parent_id,
		parent_name,
		stock_amount,
		status,
		create_time,
		update_time,
		creator
	)
	values
	(
		:stock_name,
		:stock_level,
		:parent_id,
		:parent_name,
		:stock_amount,
		:status,
		:create_time,
		:update_time,
		:creator
	)`, "stock")

	args := map[string]interface{}{}
	args["stock_name"] = reqParam.StockName
	args["parent_id"] = reqParam.ParentID
	args["parent_name"] = reqParam.ParentName
	args["stock_amount"] = reqParam.StockAmount
	args["status"] = reqParam.Status
	args["create_time"] = time.Now().Unix()
	args["update_time"] = time.Now().Unix()
	args["creator"] = reqParam.Creator

	if tx != nil {
		_, err = tx.GetTx().NamedExec(sqlStr, args)
	} else {
		_, err = dbi.NamedExec(sqlStr, args)
	}
	if err != nil {
		log.Println(err.Error())
		return err
	}

	return nil
}

//UpdateOrganInfo 更新机构信息
func UpdateStock(tx *Transaction, id, amt int64, status string) error {
	sql := fmt.Sprintf(`update stock set update_time = :update_time  `)

	if amt > 0 {
		sql += ", stock_amount = stock_amount+ :amt"
	}

	if status != "" {
		sql += ", status=:status"
	}

	args := make(map[string]interface{})
	args["amt"] = amt
	args["status"] = status
	args["update_time"] = time.Now().Unix()

	var err error
	if tx != nil {
		_, err = tx.GetTx().NamedExec(sql, args)
	} else {
		_, err = dbi.NamedExec(sql, args)
	}
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

//GetStock 查询库存
func GetStock(tx *Transaction, req *proto.GetStockRequest) (list []Stock, length int, err error) {

	args := map[string]interface{}{
		"id":          req.StockId,
		"stock_name":  req.StockName,
		"parent_id":   req.ParentId,
		"parent_name": req.ParentName,
		"update_time": req.UpdateTime,
		"page_size":   req.PageSize,
		"page_index":  req.PageIndex,
	}

	var querySQL, queryCount bytes.Buffer

	querySQL.WriteString(`SELECT * FROM stock WHERE 1=1 `)
	queryCount.WriteString(`SELECT COUNT(*) as count FROM stock WHERE 1=1 `)

	if req.StockId >= 0 {
		querySQL.WriteString(" and id=:id")
	}

	if req.StockName != "" {
		querySQL.WriteString(" and stock_name=:stock_name")
	}

	if req.ParentName != "" {
		querySQL.WriteString(" and parent_name=:parent_name")
	}

	querySQL.WriteString(`  order by id desc`)

	if req.PageIndex > 0 && req.PageSize > 0 {
		querySQL.WriteString(fmt.Sprintf(" limit %d, %d ", req.PageSize*(req.PageIndex-1), req.PageSize))
	} else {
		querySQL.WriteString(fmt.Sprintf(" limit %d, %d ", 1, 10))
	}

	sqlN, argN, err := sqlx.Named(querySQL.String(), args)
	if err != nil {
		logger.Error(err)
		return
	}
	sqlN, argN, err = sqlx.In(sqlN, argN...)
	if err != nil {
		logger.Error(err)
		return
	}
	err = GetDB().Select(&list, sqlN, argN...)
	if err != nil {
		logger.Error(err)
		return
	}

	sqlN, argN, err = sqlx.Named(queryCount.String(), args)
	if err != nil {
		logger.Error(err)
		return
	}
	sqlN, argN, err = sqlx.In(sqlN, argN...)
	if err != nil {
		logger.Error(err)
		return
	}

	err = GetDB().Get(&length, sqlN, argN...)
	if err != nil {
		logger.Error(err)
		return
	}

	return
}
