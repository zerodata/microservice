package handler

import (
	"errors"
	"stockcenter/db/mysql"
	"stockcenter/proto"

	"golang.org/x/net/context"
)

type HandlerV4 struct{}

//增加库存
func (h *HandlerV4) AddStock(ctx context.Context, req *proto.AddStockRequest) (*proto.AddStockReply, error) {
	resp := proto.AddStockReply{}
	resp.CodeMsg = new(proto.BaseReply)

	if req.StockId <= 0 {
		resp.CodeMsg.Code = 10001
		resp.CodeMsg.Msg = ""
		return &resp, errors.New("参数错误")
	}

	mysql.UpdateStock(nil, req.StockId, req.StockAmt, "")

	resp.CodeMsg.Code = 0
	resp.CodeMsg.Msg = "success"
	return &resp, nil
}

//查询库存
func (h *HandlerV4) GetStock(ctx context.Context, req *proto.GetStockRequest) (*proto.GetStockReply, error) {
	resp := proto.GetStockReply{}
	resp.CodeMsg = new(proto.BaseReply)

	resp.CodeMsg.Code = 0
	resp.CodeMsg.Msg = "success"
	return &resp, nil
}

//转移库存
func (h *HandlerV4) TransferStock(ctx context.Context, req *proto.TransferStockRequest) (*proto.TransferStockReply, error) {
	resp := proto.TransferStockReply{}
	resp.CodeMsg = new(proto.BaseReply)

	resp.CodeMsg.Code = 0
	resp.CodeMsg.Msg = "success"
	return &resp, nil
}

//更新库存状态
func (h *HandlerV4) UpdateStockStatus(ctx context.Context, req *proto.UpdateStockStatusRequest) (*proto.UpdateStockStatusReply, error) {
	resp := proto.UpdateStockStatusReply{}
	resp.CodeMsg = new(proto.BaseReply)

	mysql.UpdateStock(nil, req.StockId, 0, req.Status)

	resp.CodeMsg.Code = 0
	resp.CodeMsg.Msg = "success"
	return &resp, nil
}
