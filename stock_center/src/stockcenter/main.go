package main

import (
	"log"
	"net"
	"stockcenter/db/mysql"
	"stockcenter/handler"
	"stockcenter/proto"

	"google.golang.org/grpc"
)

func main() {
	mysql.InitDB()

	runGRPCServer()
}

func runGRPCServer() {
	lis, err := net.Listen("tcp", ":8089")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	// err = grpclb.Register("http://127.0.0.1:2379", "newscenter", "localhost", config.Config.RpcPort, time.Second*10, 15)
	// if err != nil {
	// 	panic(err)
	// }

	//增加限流器
	opts := []grpc.ServerOption{
		// grpc_middleware.WithUnaryServerChain(
		// 	util.CurrentLimitInterceptor,
		// ),
	}

	s := grpc.NewServer(opts...)
	proto.RegisterStockCenterServer(s, &handler.HandlerV4{})
	go func() {
		if err := s.Serve(lis); err != nil {
			log.Fatal(err)
		}
	}()
}
