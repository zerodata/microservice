module stockcenter

go 1.13

require (
	github.com/gin-gonic/gin v1.4.0
	github.com/go-xorm/xorm v0.7.9
	github.com/golang/protobuf v1.3.2
	github.com/jmoiron/sqlx v1.2.0
	github.com/smmit/smmbase v1.2.59
	golang.org/x/net v0.0.0-20190503192946-f4e77d36d62c
	google.golang.org/grpc v1.25.1
)
