
CREATE TABLE IF NOT EXISTS `stock` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stock_name` varchar(50) NOT NULL COMMENT '库存名称',
  `parent_id` int(10) NOT NULL DEFAULT 0 COMMENT '一级分类id',
  `parent_name` varchar(50) NOT NULL COMMENT '一级分类id名称',
  `stock_amount` int(10) NOT NULL DEFAULT 0 COMMENT '库存数量',
  `transfer_id` int(10) NOT NULL DEFAULT 0 COMMENT '转移库存id',
  `status` varchar(2) NOT NULL DEFAULT "00" COMMENT '状态 00:已启用 01:已禁用',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '编辑时间',
  `creator` varchar(50) NOT NULL COMMENT '创建人',
  PRIMARY KEY (`id`),
  KEY `stock_stock_name` (`stock_name`),
  KEY `stock_status` (`status`),
) ENGINE=InnoDB DEFAULT CHARSET=utf8;